# Provisioner

---

### Customization

Generate a Crypted Password:

    mkssh_userpass --method=SHA-512

### Deployment

Example Command Line:

    ansible-playbook playbook.yml -l dev -k

    ansible-playbook -i [inventory-file] [playbook]
    ansible-playbook -i hosts playbook.yml
    ansible-playbook -i hosts playbook.yml --ask-sudo-pass
    ansible-playbook -i -vvv  playbook.yml --tags ntp
    ansible-playbook playbook.yml --skip-tags "notification"

Reconfigure whole infrastructure

    ansible-playbook -i production site.yml

Reconfiguring NTP tags on everything:

    ansible-playbook -i -vvv production site.yml --tags ntp

Reconfiguring my webservers only:

    ansible-playbook -i production webservers.yml

Reconfiguring my webservers only in Boston:

    ansible-playbook -i production webservers.yml --limit boston

Ad-Hoc Commands:

    ansible boston -i production -m ping
    ansible boston -i production -m command -a '/sbin/reboot'

### Ansible Instructions

Local environment can edit the following: `vim /etc/ansible/hosts`. You append your hosts to this file. Alternatively, you can create a file in your repository called `hosts` alongside `yml` file. An example of a host file with and without a port:

    site-a.com
    site-b.com:3333

For multiple servers we can group them in `[brackets]`. Additionally, we can add more details, such as allow host names (for playbooks) and set target values:

    [websites]
    hosta   site-a.com
    hostb   site-b.com:3333

    [targets]
    localhost       ansible_connection=local
    site-a.com      ansible-connection=ssh
    site-a.com      ansible_connection=ssh

We can then create our `playbook.yml` and target the `websites` all in one configuration if we chose to, eg:

    ---
    - hosts: websites
      vars:
        http_port: 80
        max_clients: 200
    ..etc..

## Executing a playbook

This will run the playbook on a parallelism level of 10:

    ansible-playbook playbook.yml -f 10

If you have a `hosts` file alongside your ansible playbook you can run:

    ansible-playbook -i hosts playbook.yml




